<?php

namespace App;

use App\States\DevReviewTicketState;
use App\States\InProgressTicketState;
use App\States\NeedTestTicketState;
use App\States\OpenTicketState;
use App\States\ReOpenedTicketState;
use App\States\ResolvedTicketState;
use App\States\TestingTicketState;
use App\States\TicketState;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStates\HasStates;

class Ticket extends Model
{
    use HasStates;

    protected $table = 'tickets';
    protected $fillable = [
        'title',
        'status'
    ];

    protected function registerStates(): void
    {
        $this
            ->addState('status', TicketState::class)
            ->default(OpenTicketState::class)
            ->allowTransitions([
                [OpenTicketState::class, InProgressTicketState::class],
                [OpenTicketState::class, NeedTestTicketState::class]
            ])
            ->allowTransitions([
                [InProgressTicketState::class, DevReviewTicketState::class]
            ])
            ->allowTransitions([
                [DevReviewTicketState::class, NeedTestTicketState::class]
            ])
            ->allowTransitions([
                [NeedTestTicketState::class, TestingTicketState::class]
            ])
            ->allowTransitions([
                [TestingTicketState::class, ResolvedTicketState::class],
                [TestingTicketState::class, ReOpenedTicketState::class]
            ])
            ->allowTransitions([
                [ReOpenedTicketState::class, NeedTestTicketState::class],
                [ReOpenedTicketState::class, InProgressTicketState::class]
            ]);
    }
    public function getTransitionableStates(){
        return $this->status->transitionableStates();
    }
}
