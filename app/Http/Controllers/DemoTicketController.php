<?php

namespace App\Http\Controllers;

use App\States\TicketState;
use App\Ticket;
use Illuminate\Http\Request;

class DemoTicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::all();

        return view('demoTicket', compact('tickets'));
    }

    public function updateStatus(Request $request, $id)
    {
        $title         = $request->input('TicketTitle');
        $ticket        = Ticket::find($id);
        $status        = $request->input('status');
        $ticket->title = $title;
        $ticket->save();
        if ($status != null) {
            $newStatus = TicketState::find($status, $ticket);
            $ticket->status->transitionTo($newStatus);
        }

        return redirect()->route('ticket.index');
    }

    public function deleteTicket($id)
    {
        $ticket = Ticket::find($id);
        $ticket->delete();

        return redirect()->route('ticket.index');
    }

    public function createTicket(Request $request)
    {
        $title  = $request->title;
        $status = $request->input('TicketStatus');
        Ticket::create([
            'title'  => $title,
            'status' => $status
        ]);

        return redirect()->route('ticket.index');
    }
}
