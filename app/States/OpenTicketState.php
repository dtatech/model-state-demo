<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class OpenTicketState extends TicketState
{
    public static $name = 'Open';
}
