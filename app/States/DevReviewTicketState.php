<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class DevReviewTicketState extends TicketState
{
    public static $name = 'DevReview';
}
