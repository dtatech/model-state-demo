<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class ResolvedTicketState extends TicketState
{
    public static $name = 'Resolved';
}
