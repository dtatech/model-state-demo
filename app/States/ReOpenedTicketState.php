<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class ReOpenedTicketState extends TicketState
{
    public static $name = 'ReOpened';
}
