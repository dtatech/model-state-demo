<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;
class TestingTicketState extends TicketState
{
    public static $name = 'Testing';
}
