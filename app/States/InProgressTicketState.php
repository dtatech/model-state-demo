<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class InProgressTicketState extends TicketState
{
    public static $name = 'InProgress';
}
