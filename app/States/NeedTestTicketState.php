<?php

namespace App\States;

use Spatie\ModelStates\State;
use App\States\TicketState;

class NeedTestTicketState extends TicketState
{
    public static $name = 'NeedTest';
}
