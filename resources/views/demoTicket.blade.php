<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title>Demo Ticket</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
            crossorigin="anonymous"></script>
</head>
<body>
<table>
    <thead>
    <tr>
        <th scope="col" class="text-white text-center">STT</th>
        <th scope="col" class="text-white text-center">Tên Ticket</th>
        <th scope="col" class="text-white text-center">Trạng thái</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tickets as $ticket)
        <tr class="">
            <td class="text-center">{{$loop->iteration}}</td>
            <td>
                <form method="POST" action="{{route('ticket.update',['id'=> $ticket->id])}}">
                    @csrf
                    @method('PATCH')
                    <div class="d-flex">
                        <input type="text" name="TicketTitle" value="{{ $ticket->title }}">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                                    id="{{$ticket->id}}">
                                <span class="caret">{{$ticket->status}}</span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($ticket->getTransitionableStates() as $ticketTransition)
                                    <li><a class="{{$ticket->id}}" href="#">{{$ticketTransition}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <input type="hidden" value="" id="status" name="status">
                        <button type="submit">Sửa</button>
                    </div>
                </form>
            </td>
            <td>
                <form method="POST" action="{{route('ticket.update',['id'=> $ticket->id])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Xóa</button>
                </form>
            </td>
        </tr>
    @endforeach
    <tr>
        <form method="GET" action="{{route('ticket.create')}}">
            @csrf
            <input type="text" value="" id="title" name="title">
            <select id="TicketStatus" name="TicketStatus">
                <option value="Open" selected>Open</option>
                <option value="InProgress">InProgress</option>
                <option value="DevReview">DevReview</option>
                <option value="NeedTest">NeedTest</option>
                <option value="Testing">Testing</option>
                <option value="ReOpened">ReOpened</option>
                <option value="Resolved">Resolved</option>
            </select>
            <button type="submit">Thêm</button>
        </form>
    </tr>
    </tbody>
</table>
</body>
<script>
    $(".dropdown-menu li a").click(function () {
        let selText = $(this).text();
        let className = $(this).attr('class');
        $("#" + className).html(selText);
        $("#status").val(selText);
    });
</script>
</html>
