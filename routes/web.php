<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/demo-ticket', 'DemoTicketController@index')->name('ticket.index');
Route::patch('/demo-ticket/{id}', 'DemoTicketController@updateStatus')->name('ticket.update');
Route::delete('/demo-ticket/{id}', 'DemoTicketController@deleteTicket')->name('ticket.delete');
Route::get('/demo-ticket/create', 'DemoTicketController@createTicket')->name('ticket.create');
